package skiplist

import "unsafe"

const (
	maxNodeSize = int(unsafe.Sizeof(Node{}))
	maxHeight   = 5
	nodePadding = int(unsafe.Sizeof(uint32(0)) - 1)
	offsetSize  = int(unsafe.Sizeof(uint32(0)))
)

type Skiplist struct {
	max_level int
	level     int
	head      *Node
}
