package skiplist

import "sync/atomic"

type Node struct {
	keyOffset uint32
	keySize   uint16
	// Will hold two parts, the offset, and the valSize
	value atomic.Uint64

	height uint16

	forward []atomic.Uint32
}

func NewNode(a *Arena, key, val []byte, height int) uint32 {
	// TODO:: Probably should check some shit here?
	offset := a.newNode(height)
	n := a.getNode(offset)
	n.keyOffset = a.putKey(key)
	n.keySize = uint16(len(key))
	n.value.Store(packTo64(a.putValue(val, n), uint32(len(val))))

	return offset
}

func packTo64(off, size uint32) uint64 {
	return uint64(size)<<32 | uint64(off)
}

func decodeVal(val uint64) (off uint32, size uint32) {
	off = uint32(val)
	size = uint32(val >> 32)
	return
}
