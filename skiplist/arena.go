package skiplist

import (
	"fmt"
	"log"
	"sync/atomic"
	"unsafe"
)

// Lock free arena
type Arena struct {
	size atomic.Uint32
	buf  []byte
}

func NewArena(size int) *Arena {
	buffer := make([]byte, size)

	a := &Arena{
		buf: buffer,
	}

	a.size.Add(0)
	return a
}

func (a *Arena) newNode(height int) uint32 {
	// Don't store a full forward array if we don't need too
	unused := (maxHeight - height) * offsetSize

	max := int(unsafe.Sizeof(Node{}))
	fmt.Printf("Max: %d\n", max)

	real := uint32((max - unused) + nodePadding)
	fmt.Printf("RealSize: %d\n", real)

	curr := a.size.Load()
	req := real + curr
	// TODO:: maybe we check that req isn't longer than a.size instead
	if req > (1<<32)-1 || req >= uint32(len(a.buf)) {
		log.Fatalf("Arena max size exceeded. Requested=%d\n", req)
	}

	n := a.size.Add(req)

	offset := ((n - real) + uint32(nodePadding)) & ^uint32(nodePadding)

	fmt.Printf("Computed offset: %d\n", offset)
	return offset
}

// Returns keyOffset
func (a *Arena) putKey(key []byte) uint32 {
	l := uint32(len(key))
	c := a.size.Add(l)

	if c >= uint32(len(a.buf)) {
		log.Fatalf("Requested size will be too large: %d\n", c)
	}

	o := c - l

	copy(a.buf[o:c], key)
	return o
}

func (a *Arena) putValue(val []byte, node *Node) uint32 {
	// maxNodeSize - unused is where the keyGoes
	// Value goes after key
}

func (a *Arena) getNode(offset uint32) *Node {
	return (*Node)(unsafe.Pointer(&a.buf[offset]))
}
