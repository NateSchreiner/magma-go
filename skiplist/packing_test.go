package skiplist

import (
	"fmt"
	"testing"
)

type TestCase struct {
	address int
	size    int
}

func Test64BitPack(t *testing.T) {
	testCases := make([]TestCase, 0)

	testCases = append(testCases, TestCase{address: 123456, size: 500})
	testCases = append(testCases, TestCase{address: 1234567789098, size: 0})
	testCases = append(testCases, TestCase{address: 1, size: 12345566776})
	testCases = append(testCases, TestCase{address: 0, size: 0})
	testCases = append(testCases, TestCase{address: 9223372036854775807, size: 9223372036854775807})

	// for _ case := testCases {

	// }

}

func TestPackSingle(t *testing.T) {
	var addr uint32 = 5
	var size uint32 = 10
	var test uint64 = uint64(size)<<32 | uint64(addr)

	first := uint32(test)
	second := uint32(test >> 32)

	fmt.Printf("First: %d.. Second: %d\n", first, second)
}
