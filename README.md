# Magma-go 

## Durable K/V storage engine

### Based of of Couchbases Magma 
> https://www.vldb.org/pvldb/vol15/p3496-lakshman.pdf


#### Implementation 

#### Write Operation
1. "Each document mutation has a monotonically increasing sequence number"
2. First inserted into **active** write-cache. Write-cache contains two skip-lists
    - One indexed by key, the other by seq num
3. Written to WAL, write thread returns success status after writing WAL

### Implementation Details
> https://smalldatum.blogspot.com/2022/09/magma-new-storage-engine-for-couchbase.html

It was time for something new and I like the replacement - Magma. Goals for it include:
    concurrent, incremental compaction
    less write-amplification
    optimized for SSD (sustain more concurrent IO requests)
    support deployments with 100:1 ratios for data:memory
    avoid the fatal flaw of index+log (don't do index lookups during GC)

The basic workload for a Couchbase storage engine is:
    point read of documents by primary key
    range scan of documents by seqno (commit sequence number) for change feeds
    upsert N documents
    while this requires two indexes (by PK, by seqno) secondary indexes on document attributes is provided by another service
    average document size is between 1kb and 16kb

#### TODO:: 
1. Write skip-lists
