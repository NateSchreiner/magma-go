package main

import (
	"bytes"
	"fmt"
)

func compareSlices() {
	first := []byte("abc")
	second := []byte("abcd")

	result := bytes.Compare(first, second)
	if result >= 0 {
		fmt.Printf("%s lesss than: %s\n", first, second)
	} else {
		fmt.Printf("%s greater than: %s\n", second, first)
	}

}

func main() {
	var max64 uint64 = (1 << 64) - 1
	fmt.Printf("max: %d\n", max64)

	var max32 uint32 = (1 << 32) - 1
	fmt.Printf("max: %d\n", max32)

	var five uint32 = 5
	// var six uint64 = 0
	six := uint64(five) >> 32

	fmt.Printf("Still five: %d\n", uint32(six))

}
